<?php

/**
 * Network interface settings view.
 *
 * @category   apps
 * @package    network
 * @subpackage views
 * @author     ClearFoundation <developer@clearfoundation.com>
 * @copyright  2011 ClearFoundation
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/network/
 */

///////////////////////////////////////////////////////////////////////////////
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.  
//
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// Load dependencies
///////////////////////////////////////////////////////////////////////////////

use \clearos\apps\network\Iface as Iface;
use \clearos\apps\network\Role as Role;

if (clearos_library_installed('dhcp/Dnsmasq')) {
    $this->load->library('dhcp/Dnsmasq');
    $subnets = $this->dnsmasq->get_subnets();
}

$this->load->language('base');
$this->load->language('network');
$this->load->library('network/Iface_Manager');

// Get Bridged Interfaces
$output = shell_exec(" cat /etc/sysconfig/network-scripts/ifcfg-br* | grep  -oP '(?<=DEVICE=).*' ");
$output = preg_split('/\s+/', trim($output));
$bridge_options['empty'] = 'Select Bridge';
foreach ($output as &$line) {
    $bridge_options[$line] = $line;
}

///////////////////////////////////////////////////////////////////////////////
// Form modes
///////////////////////////////////////////////////////////////////////////////

if ($form_type === 'edit') {
    $read_only = FALSE;
    $form_path = '/network/iface/edit/' . $interface;
    $buttons = array(
        form_submit_update('submit'),
        anchor_cancel('/app/network/iface/'),
        anchor_delete('/app/network/iface/delete/' . $interface)
    );
} else if ($form_type === 'add') {
    $read_only = FALSE;
    $form_path = '/network/iface/add/' . $interface;
    $buttons = array(
        form_submit_add('submit'),
        anchor_cancel('/app/network/iface/'),
    );
} else {
    $read_only = TRUE;
    $form_path = '';
    $buttons = array(
        anchor_cancel('/app/network/iface/'),
    );
}

$mac = empty($iface_info['hwaddress']) ? '' : $iface_info['hwaddress'];
$bus = empty($iface_info['bus']) ? '' : $iface_info['bus'];
$vendor = empty($iface_info['vendor']) ? '' : $iface_info['vendor'];
$device = empty($iface_info['device']) ? '' : $iface_info['device'];
$link = (isset($iface_info['link']) && $iface_info['link']) ? lang('base_yes') : lang('base_no');
$speed = (isset($iface_info['speed']) && ($iface_info['speed'] > 0)) ? $iface_info['speed'] . ' ' . lang('base_megabits_per_second') : lang('base_unknown');
$dns = (isset($iface_info['ifcfg']['peerdns'])) ? $iface_info['ifcfg']['peerdns'] : TRUE;
$minions = isset($iface_info['minions']) ? $iface_info['minions'] : [];

$bootproto_read_only = (isset($iface_info['type']) && $iface_info['type'] === Iface::TYPE_PPPOE) ? TRUE : $read_only;

///////////////////////////////////////////////////////////////////////////////
// Form open
///////////////////////////////////////////////////////////////////////////////

echo form_open($form_path);
echo form_header(lang('network_interface'));
echo "<input type='hidden' id='network_interface_type' value='$type'>";

///////////////////////////////////////////////////////////////////////////////
// General information
///////////////////////////////////////////////////////////////////////////////

echo fieldset_header(lang('base_information'));

echo field_input('type_current', $iface_info['type'], 'Type', TRUE);

if ($mac)
    echo field_input('mac', $mac, lang('network_mac_address'), TRUE);

if ($vendor)
    echo field_input('vendor', $vendor, lang('network_vendor'), TRUE);

if ($device)
    echo field_input('device', $device, lang('network_device'), TRUE);

if ($bus)
    echo field_input('bus', $bus, lang('network_bus'), TRUE);

echo field_input('link', $link, lang('network_link'), TRUE);
echo field_input('speed', $speed, lang('network_speed'), TRUE);

// Add PPPoE hardware interface information
if (isset($iface_info['type']) && $iface_info['type'] === Iface::TYPE_PPPOE)
    echo field_input('minion', $minions[0], lang('network_hardware_interface'), TRUE);

echo fieldset_footer();

if (isset($iface_info['type']) && $iface_info['type'] === Iface::TYPE_BRIDGED) {
    echo fieldset_header(lang('network_interfaces_in_bridge'));
    for ($inx = 0; $inx < count($minions); $inx++)
        echo field_input('minion_' . $minions[$inx], $minions[$inx], lang('network_hardware_interface') . ' #' . ($inx + 1), TRUE);
    echo fieldset_footer();
}

$bootproto = $iface_info['ifcfg']['bootproto']; 
if ($iface_info['type'] === Iface::TYPE_BRIDGED_SLAVE) {
    echo field_input('current_bridge', $iface_info['ifcfg']['bridge'], lang('network_bridge_interface'), TRUE);
    $bootproto = 'bridged';
}

///////////////////////////////////////////////////////////////////////////////
// Settings
///////////////////////////////////////////////////////////////////////////////

$slaves = [
    Iface::TYPE_BRIDGED_SLAVE,
    Iface::TYPE_BONDED_SLAVE,
    Iface::TYPE_PPPOE_SLAVE,
];

echo fieldset_header(lang('base_settings'));
echo field_input('interface', $interface, lang('network_interface'), TRUE);

if (in_array($iface_info['type'], $slaves)) {
    if (isset($iface_info['type']) && $iface_info['type'] === Iface::TYPE_BRIDGED_SLAVE)
        echo field_input('master_current', $iface_info['ifcfg']['bridge'], 'Current Bridge', TRUE);
    echo fieldset_footer();
} else {
    // Common settings
    //----------------

    echo field_dropdown('role', $roles, $iface_info['role'], lang('network_role'), $read_only, array('id' => 'role'));

    echo field_dropdown('bootproto', $bootprotos, $bootproto, lang('network_connection_type'), $bootproto_read_only);
    echo field_dropdown('bridge_master', $bridge_options, $iface_info['ifcfg']['bridge'], Bridge, $read_only);

    // Static
    //-------

    echo field_input('ipaddr', $iface_info['ifcfg']['ipaddr'], lang('network_ip'), $read_only);
    echo field_input('netmask', $iface_info['ifcfg']['netmask'], lang('network_netmask'), $read_only);
    echo field_input('gateway', $iface_info['ifcfg']['gateway'], lang('network_gateway'), $read_only);

    if ($show_dhcp)
        echo field_checkbox('enable_dhcp', $enable_dhcp, lang('network_enable_dhcp_server'), $read_only);

    if (clearos_library_installed('dhcp/Dnsmasq') && $subnets[$interface]['isconfigured'] == TRUE )
        echo field_checkbox('enable_dhcp', 'y', lang('network_update_dhcp_server'), $read_only);  

    // DHCP
    //-----

    echo field_input('hostname', $iface_info['ifcfg']['dhcp_hostname'], lang('network_hostname'), $read_only);
    echo field_checkbox('dhcp_dns', $dns, lang('network_automatic_dns_servers'), $read_only);

    // PPPoE
    //------

    echo field_input('username', $iface_info['ifcfg']['user'], lang('base_username'), $read_only);
    echo field_input('password', $password, lang('base_password'), $read_only);
    echo field_input('mtu', $iface_info['ifcfg']['mtu'], lang('network_mtu'), $read_only);
    echo field_checkbox('pppoe_kernel', $iface_info['ifcfg']['linux_plugin'], 'Kernel Mode', $read_only);
    echo field_checkbox('pppoe_dns', $dns, lang('network_automatic_dns_servers'), $read_only);

    ///////////////////////////////////////////////////////////////////////////////
    // Maximum Upload/Download
    ///////////////////////////////////////////////////////////////////////////////

    echo fieldset_header(lang('network_maximum_bandwith_available'), array('id' => 'fieldset_header_bandwidth'));
    echo field_input('max_upstream', $max_upstream, lang('network_upstream') . ' (' . lang('base_kilobits_per_second') . ')', $read_only);
    echo field_input('max_downstream', $max_downstream, lang('network_downstream') . ' (' . lang('base_kilobits_per_second') . ')', $read_only);
    echo fieldset_footer();

    ///////////////////////////////////////////////////////////////////////////////
    // Upstream Proxy
    ///////////////////////////////////////////////////////////////////////////////

    echo fieldset_header(lang('network_upstream_proxy'), array('id' => 'fieldset_header_upstream_proxy'));
    echo field_input('proxy_server', $proxy_server, lang('network_proxy_server'), $read_only);
    echo field_input('proxy_port', $proxy_port, lang('network_port'), $read_only);
    echo field_input('proxy_username', $proxy_username, lang('base_username'), $read_only);
    echo field_input('proxy_password', $proxy_password, lang('base_password'), $read_only);
    echo fieldset_footer();
}

///////////////////////////////////////////////////////////////////////////////
// Common footer
///////////////////////////////////////////////////////////////////////////////

echo field_button_set($buttons);

///////////////////////////////////////////////////////////////////////////////
// Form close
///////////////////////////////////////////////////////////////////////////////

echo form_footer();
echo form_close();
